---
Title: Poursuivre les débats sur le Net, quelles solutions?
Date: 2018-01-07
---

# Alternatives aux outils existants

Nommer les outils que vous utilisez :

* Facebook, twitter, listes de discussion, forums, commentaires, …

Comment vous souhaitez débattre ? phrases courtes, discours plus long, chronologique, par thématique, …

## Comme Facebook

Comme Facebook : Diaspora – instance [Framasphère](https://framasphere.org/)

Avec des trucs plus sympa que sur FB : l'ajout de vote, l'édition en texte riche, plus de possibilité de suivre des thématiques

### Discussions au fil de l'eau (comme twitter)

Comme twitter : Mastodon – instance [Framapiaf](http://framapiaf.org/) ou [mastodon.tetaneutral](http://mastodon.tetaneutral.net/)

Plus de caractères que sur Tw, le choix de diffuser vos messages à tous, à vos abonnés seulement ou aux destinataires seuls.

### Discussion de salons

Comme Slack : Mattermost – instance [Framateam](https://framateam.org/), mais d'autres outils existent. Créez votre équipe, ajoutez des salons publics ou privés, discutez en pair à pair. Remplace avantageusement les listes de discussion ou les forums, sauf si vous préférez les longs discours aux dialogues. Très utile pour vous organiser.

## Forums

Les forums reviennent plus modernes, par exemple [celui des communs](http://forum.lescommuns.org/) ou [framacolibri](http://framacolibri.org/). Mais pas d'instance "publique" comme pour les réseaux sociaux, toujours des forums dédiés à des thématiques données.

## Prendre des décisions

Loomio – instance [Framavox](http://framavox.org/)

## Discuter en direct à peu de personnes

Discussion audio ou vidéo avec [Framatalk](http://framatalk.org/)

## Le couteau suisse : Communecter

[Communecter](https://www.communecter.org/), notamment utilisé par [Alternatiba Pei](https://www.communecter.org/#alternatibaPei.view.directory.dir.members) à la Réunion. "Réseau sociétal", news, pages d'organisation, projet, agenda, outil de décision, fichiers partagers, cartographie…

# Ressources

* Framasoft et les services proposés : [https://degooglisons-internet.org](https://degooglisons-internet.org/)
* Le collectif des hébergeurs alternatifs : [CHATONS](https://chatons.org/)
